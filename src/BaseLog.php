<?php

namespace Pugpig\WordPressUtils;

abstract class BaseLog
{
    const LOG_STATUS_INFO = 'info';
    const LOG_STATUS_DEBUG = 'debug';
    const LOG_STATUS_WARNING = 'warning';
    const LOG_STATUS_ERROR = 'error';
    const LOG_STATUS_SUCCESS = 'success';

    /** @param array<string, mixed> $context */
    public function info(string $message, array $context=[]): void
    {
        $this->log(static::LOG_STATUS_INFO, $message, $context);
    }

    /** @param array<string, mixed> $context */
    public function debug(string $message, array $context=[]): void
    {
        $this->log(static::LOG_STATUS_DEBUG, $message, $context);
    }

    /** @param array<string, mixed> $context */
    public function warning(string $message, array $context=[]): void
    {
        $this->log(static::LOG_STATUS_WARNING, $message, $context);
    }

    /** @param array<string, mixed> $context */
    public function error(string $message, array $context=[]): void
    {
        $this->log(static::LOG_STATUS_ERROR, $message, $context);
    }

    /** @param array<string, mixed> $context */
    public function success(string $message, array $context=[]): void
    {
        $this->log(static::LOG_STATUS_SUCCESS, $message, $context);
    }

    /** @param array<string, mixed> $context */
    public function __invoke(string $message, array $context=[]): void
    {
        $this->info($message, $context);
    }

    /** @param array<string, mixed> $context */
    abstract public function log(string $level, string $message, array $context=[]): void;
}
