<?php

namespace Pugpig\WordPressUtils;

// require_once ABSPATH . 'wp-admin/includes/plugin.php';

class DependencyChecker
{
    const MINIMUM_WORDPRESS_VERSION = '5.0.0';

    const NEEDED_PLUGINS = [
        [
            'leafname' => 'pugpig.php',
            'name' => 'Pugpig - App',
        ],
    ];

    const NEEDED_DEFINES = [];

    /**
     * @var string $title
     */
    protected static $title;

    public static function hasAllDependencies(): bool
    {
        return static::checkWordpressVersion(static::MINIMUM_WORDPRESS_VERSION)
            && static::checkNeededDefines(static::NEEDED_DEFINES)
            && static::checkRequiredPlugins(static::NEEDED_PLUGINS);
    }

    protected static function title(): string
    {
        if (empty(static::$title)) {
            // todo: support this file being outside of the plugin
            $plugin_info = PluginUtils::getPluginInfoFromFilepath(__FILE__);
            if (empty($plugin_info)
                || !isset($plugin_info['Title'])
                || !is_string($plugin_info['Title'])
            ) {
                return 'Plugin';
            }
            static::$title = $plugin_info['Title'];
        }
        return static::$title;
    }

    protected static function checkWordpressVersion(string $required_wp_version): bool
    {
        global $wp_version;

        // todo: extend to support checking for maximum wordpress version
        if (version_compare($wp_version, $required_wp_version) === -1) {
            static::displayMessage(static::title() . " requires Wordpress version <code>{$required_wp_version}</code> or later");
            return false;
        }
        return true;
    }

    /**
     * @param string[] $needed_defines
     */
    protected static function checkNeededDefines(array $needed_defines): bool
    {
        $all_defined = true;
        foreach ($needed_defines as $define_key => $define_description) {
            if (!defined($define_key)) {
                static::displayMessage(static::title() . " needs <code title=\"{$define_description}\">{$define_key}</code> to be defined.");
                $all_defined = false;
            }
        }
        return $all_defined;
    }

    /**
     * @param array<int, array<string, string>> $needed_plugins
     */
    protected static function checkRequiredPlugins(array $needed_plugins): bool
    {
        if (!function_exists('get_plugins')) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }

        $available_plugin_paths = array_keys(get_plugins());

        $all_present = true;
        foreach ($needed_plugins as $needed_plugin_info) {
            $plugin_found_and_active = false;

            $plugin_info = PluginUtils::getPluginInfoByMainFileLeafname($needed_plugin_info['leafname']);
            if (!is_null($plugin_info)) {
                // todo: optionally check version number here
                $plugin_found_and_active = is_string($plugin_info['filepath'])
                    ? is_plugin_active($plugin_info['filepath'])
                    : false;
            }

            if (!$plugin_found_and_active) {
                static::displayMessage(static::title() . " requires the plugin {$needed_plugin_info['name']}.  Please activate it.");
                $all_present = false;
            }
        }

        return $all_present;
    }

    protected static function displayMessage(string $message): void
    {
        $display_message_function = function () use ($message) {
            echo "<div class=\"error\"><p>{$message}</p></div>";
        };

        add_action('network_admin_notices', $display_message_function);
        add_action('admin_notices', $display_message_function);
    }
}
