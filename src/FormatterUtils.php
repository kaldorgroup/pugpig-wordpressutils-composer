<?php

namespace Kaldor\PdfPageImporter\Base;

class FormatterUtils
{
    public static function humanSize(int $bytes, int $decimal_places = 2, bool $use_ieee_1541 = true): string
    {
        if ($use_ieee_1541) {
            $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];
        } else {
            // otherwise use JEDEC Standard 100B.01
            $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        }

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }
        return round($bytes, $decimal_places) . ' ' . $units[$i];
    }
}
