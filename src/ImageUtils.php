<?php

namespace Pugpig\WordPressUtils;

class ImageUtils
{
    /**
     * @return int|\WP_Error|null
     */
    public static function fetchAndAttachHashedImage(int $post_id, string $image_url, ?string $title = null, bool $generate_metadata = true)
    {
        $path = parse_url($image_url, PHP_URL_PATH);
        if (empty($path)) {
            return null;
        }
        $filename = pathinfo($path, PATHINFO_FILENAME);

        $full_local_attachment_filepath = static::downloadFile($image_url, true, substr($filename, 0, 64));
        if (!is_string($full_local_attachment_filepath)) {
            return null;
        }

        return static::attachImageToPost($post_id, $full_local_attachment_filepath, $title ?? $filename, $generate_metadata);
    }

    /**
     * @return int|\WP_Error
     */
    public static function attachImageToPost(int $post_id, string $local_filepath, string $title, bool $generate_metadata = true)
    {
        $image_id = static::insertAttachment($title, $local_filepath, $post_id, $generate_metadata);
        if (!$image_id instanceof \WP_Error) {
            $image_url = wp_get_attachment_url($image_id);
            update_post_meta($image_id, 'guid', $image_url);
        }

        return $image_id;
    }

    /**
     * @return int|\WP_Error
     */
    protected static function insertAttachment(string $title, string $path, int $post_id, bool $generate_metadata = false)
    {
        if (!function_exists('wp_generate_attachment_metadata')) {
            require_once(ABSPATH . 'wp-admin/includes/media.php');
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            require_once(ABSPATH . 'wp-admin/includes/image.php');
        }

        $att_id = wp_insert_attachment([
            'post_content'      => '',
            'post_title'        => $title,
            'post_status'       => 'inherit',
            'post_mime_type'    => 'image/jpg'
        ], $path, $post_id);

        if ($generate_metadata) {
            $att_meta = wp_generate_attachment_metadata($att_id, $path);
            wp_update_attachment_metadata($att_id, $att_meta);
        }

        return $att_id;
    }

    /**
     * @return string|\WP_Error|null
     */
    protected static function downloadFile(string $url, bool $use_file_hash, string $filename_prefix)
    {
        $path = parse_url($url, PHP_URL_PATH);
        if (is_null($path) || $path===false) {
            return null;
        }

        $trimmed_path = ltrim(str_replace('wp-content/uploads/', '', $path), '/');
        
        $response = wp_remote_get($url, ['timeout'=> 20,]);

        if ($response instanceof \WP_Error) {
            return $response;
        }

        $status_code = wp_remote_retrieve_response_code($response);

        if ($status_code>=400) {
            return null;
        }

        $content = wp_remote_retrieve_body($response);

        if (empty($content)) {
            return null;
        }

        $hash = md5($content);

        $url_path_extension = pathinfo($path, PATHINFO_EXTENSION);

        $attachment_filepath_tail = $use_file_hash
            ? "{$filename_prefix}{$hash}.{$url_path_extension}"
            : rawurldecode($trimmed_path);

        $full_local_attachment_filepath = (wp_upload_dir(null, true, true)['path']) . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR . $attachment_filepath_tail;

        wp_mkdir_p(dirname($full_local_attachment_filepath));

        file_put_contents($full_local_attachment_filepath, $content);
        
        return $full_local_attachment_filepath;
    }
}
