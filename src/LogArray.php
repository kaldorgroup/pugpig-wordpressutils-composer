<?php

namespace Pugpig\WordPressUtils;

class LogArray extends BaseLog
{
    /** @var array<array<string, mixed>> $lines */
    protected $lines = [];

    /** @param array<string, mixed> $context */
    public function log(string $status, string $message, array $context=[]): void
    {
        $this->lines[] = [
            'status' => $status,
            'message' => $message,
            'context' => $context,
        ];
    }

    /** @return array<array<string, mixed>> $lines */
    public function lines(): array
    {
        return $this->lines;
    }

    public function linesAsHtml(): string
    {
        $html = '';
        foreach ($this->lines as $line) {
            if (is_string($line['status']) && is_string($line['message'])) {
                $html.= "<li class=\"log-{$line['status']}\">{$line['message']}</li>\n";
            }
        }
        return $html;
    }
}
