<?php

namespace Pugpig\WordPressUtils;

class LogConsole extends BaseLog
{
    /** @param array<string, mixed> $context */
    public function log(string $status, string $message, array $context=[]): void
    {
        $now_utc = new \DateTime('now', new \DateTimeZone('UTC'));

        echo strip_tags("{$now_utc->format(\DateTimeInterface::ATOM)} {$status}: {$message}\n");
    }
}
