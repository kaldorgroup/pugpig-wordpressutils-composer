<?php

namespace Pugpig\WordPressUtils;

class LogFile extends BaseLog
{
    /**
     * @var string $filename
     */
    protected $filename;

    public function __construct(string $leafname, string $title)
    {
        $this->filename = static::getFullLogFilename($leafname);
        $start_time = date(DATE_ATOM);
        $start_text = <<<"HTML_START"
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>{$title} - {$start_time}</title>
    <style>
        .error {
            color: #f00;
        }

        .debug {
            color: #c0c;
        }
    </style>
  </head>
  <body>
    <h1>{$title}</h1>
    <h2>{$start_time}</h2>
    <ul>
HTML_START;
        static::writeToFile($start_text);
    }

    public static function getFullLogFilename(string $leafname): string
    {
        return static::ensureLogDir() . $leafname;
    }

    public static function getLogUrl(string $leafname): string
    {
        $server_name = isset($_SERVER['SERVER_NAME']) && is_string($_SERVER['SERVER_NAME'])
            ? $_SERVER['SERVER_NAME']
            : 'unknown';

        return "/wp-content/logs/{$server_name}/{$leafname}";
    }

    /** @param array<string, mixed> $context */
    public function log(string $status, string $message, array $context=[]): void
    {
        static::addToFile("<li class=\"{$status}\">$message</li>\n");
    }

    protected function writeToFile(string $text, string $mode = 'w'): void
    {
        $file = fopen($this->filename, $mode);
        if ($file !== false) {
            fwrite($file, $text);
            fclose($file);
        }
    }

    protected function addToFile(string $text): void
    {
        static::writeToFile($text, 'a');
    }

    protected static function ensureLogDir(): string
    {
        $server_name = $_SERVER['SERVER_NAME'];
        $content_dir = defined('WP_CONTENT_DIR')
            ? WP_CONTENT_DIR
            : '.';

        $dir = $content_dir . DIRECTORY_SEPARATOR . 'logs';
        $dir .= DIRECTORY_SEPARATOR . $server_name . DIRECTORY_SEPARATOR;
        if (!file_exists($dir) && !is_dir($dir)) {
            wp_mkdir_p($dir);
        }
        return $dir;
    }
}
