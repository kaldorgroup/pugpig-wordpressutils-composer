<?php

namespace Pugpig\WordPressUtils;

class LogHtml extends BaseLog
{
    /** @param array<string, mixed> $context */
    public function log(string $status, string $message, array $context=[]): void
    {
        echo "<li class=\"{$status}\">{$message}</li>\n";
    }
}
