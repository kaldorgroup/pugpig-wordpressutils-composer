<?php

namespace Pugpig\WordPressUtils;

class LogMonoLog extends BaseLog
{
    /**
     * @var \Monolog\Logger $logger
     */
    protected $logger;

    const MONOLOG_STATUS_DEBUG = 100;
    const MONOLOG_STATUS_INFO = 200;
    const MONOLOG_STATUS_NOTICE = 250;
    const MONOLOG_STATUS_WARNING = 300;
    const MONOLOG_STATUS_ERROR = 400;
    const MONOLOG_STATUS_CRITICAL = 500;
    const MONOLOG_STATUS_ALERT = 550;
    const MONOLOG_STATUS_EMERGENCY = 600;

    public function __construct(\Monolog\Logger $logger)
    {
        $this->logger = $logger;
    }

    /** @param array<string, mixed> $context */
    public function log(string $status, string $message, array $context=[]): void
    {
        $this->logger->addRecord(
            static::getMonoLogLevel($status),
            $message,
            $context
        );
    }

    protected static function getMonoLogLevel(string $status): int
    {
        switch ($status) {
            case static::LOG_STATUS_INFO:
                return static::MONOLOG_STATUS_INFO;
            case static::LOG_STATUS_DEBUG:
                return static::MONOLOG_STATUS_DEBUG;
            case static::LOG_STATUS_WARNING:
                return static::MONOLOG_STATUS_WARNING;
            case static::LOG_STATUS_ERROR:
                return static::MONOLOG_STATUS_ERROR;
        }
        return static::MONOLOG_STATUS_INFO;
    }
}
