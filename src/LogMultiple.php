<?php

namespace Pugpig\WordPressUtils;

class LogMultiple extends BaseLog
{
    /**
     * @var BaseLog[] $loggers
     */
    protected $loggers;

    /**
     * @param BaseLog[] $loggers
     */
    public function __construct(array $loggers)
    {
        $this->loggers = $loggers;
    }

    /** @param array<string, mixed> $context */
    public function log(string $status, string $message, array $context=[]): void
    {
        foreach ($this->loggers as $logger) {
            $logger->log($status, $message, $context);
        }
    }
}
