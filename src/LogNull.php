<?php

namespace Pugpig\WordPressUtils;

class LogNull extends BaseLog
{
    /** @param array<string, mixed> $context */
    public function log(string $status, string $message, array $context=[]): void
    {
    }
}
