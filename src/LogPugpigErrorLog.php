<?php

namespace Pugpig\WordPressUtils;

class LogPugpigErrorLog extends BaseLog
{
    /** @param array<string, mixed> $context */
    public function log(string $status, string $message, array $context=[]): void
    {
        pugpig_error_log(
            htmlspecialchars_decode(
                strip_tags($message)
            ),
            $status,
            $context
        );
    }
}
