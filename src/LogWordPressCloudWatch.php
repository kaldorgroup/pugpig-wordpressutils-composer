<?php

namespace Pugpig\WordPressUtils;

class LogWordPressCloudWatch extends BaseLog
{
    /** @var mixed $logger */
    protected $logger;

    /** @param mixed $logger */
    public function __construct($logger)
    {
        $this->logger = $logger;
    }

    public function log(string $status, string $message, array $context=[]): void
    {
        $this->logger->log($status, $message, $context); /** @phpstan-ignore-line */
    }
}
