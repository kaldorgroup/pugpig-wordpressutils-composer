<?php

namespace Pugpig\WordPressUtils;

global $NAR_ALREADY_SLACKED_THIS_REQUEST;
$NAR_ALREADY_SLACKED_THIS_REQUEST = [];

class Notify
{
    public static function errorOnce(string $message, ?string $key = null): void
    {
        if (is_null($key)) {
            $key = $message;
        }
        global $NAR_ALREADY_SLACKED_THIS_REQUEST;
        if (!isset($NAR_ALREADY_SLACKED_THIS_REQUEST[$key]) || !$NAR_ALREADY_SLACKED_THIS_REQUEST[$key]) {
            Notify::error($message);
            $NAR_ALREADY_SLACKED_THIS_REQUEST[$key] = true;
        }
    }

    public static function error(string $message): void
    {
        if (!static::slack("ERROR: {$message}")) {
            trigger_error($message, E_USER_WARNING);
        }
    }

    public static function slack(string $message): bool
    {
        $configs = static::getSlackConfigs();
        $key = static::getSlackDefineKey();
        if (is_null($configs)) {
            error_log("Slack not configured (under '{$key}'), so cannot slack message [{$message}]");
            return false;
        }

        $success = true;

        foreach ($configs as $index => $config) {
            if (empty($config['url'])) {
                error_log("Slack not configured correctly - there is no `url` defined under '{$key}[{$index}]', so cannot slack message [{$message}]");
                $success = false;
                continue;
            }

            $payload = [
                'text' => $message,
                'username' => static::getUsername(),
                'icon_emoji' => 'exclamation',
            ];

            foreach (['channel', 'username', 'icon_emoji'] as $payload_config_key) {
                if (isset($config[$payload_config_key])) {
                    $payload[$payload_config_key] = $config[$payload_config_key];
                }
            }

            static::sendSlack($config['url'], $payload);
        }

        return $success;
    }

    /**
     * @param array<string, string> $payload
     */
    protected static function sendSlack(string $url, array $payload): int
    {
        $response = wp_remote_post(
            $url,
            [
                'timeout'=> 40,
                'body' => json_encode($payload),
            ]
        );

        if ($response instanceof \WP_Error) {
            $error_messages = implode(', ', $response->get_error_messages());
            error_log("Failed to post to slack hook {$url} - {$error_messages}");
            return -1;
        }

        $status_code = wp_remote_retrieve_response_code($response);
        if (is_string($status_code)) {
            $status_code = -3;
        }

        if ($status_code>=400) {
            error_log("Post to slack hook {$url} returned status {$status_code}");
            return $status_code;
        }

        $content = wp_remote_retrieve_body($response);
        error_log('slack with payload: ['. json_encode(compact('payload'))."] to hook {$url} returned {$content}");
        return $status_code;
    }

    /**
    * @return null|array<int, array<string, string>>
    */
    public static function getSlackConfigs(): ?array
    {
        $hook_define_key = static::getSlackDefineKey();
        if (defined($hook_define_key)) {
            return constant($hook_define_key); /** @phpstan-ignore-line */
        }

        return null;
    }

    protected static function getSlackDefineKey(): string
    {
        return static::getDefineKeyBase() . 'SLACK';
    }

    protected static function getDefineKeyBase(): string
    {
        return strtoupper(StringUtils::snake(static::getIdentifier()) . '_');
    }

    protected static function getIdentifier(): string
    {
        return StringUtils::snake(SiteUtils::getSiteName() . str_replace(['\\Base', '\\'], ['', '_'], __NAMESPACE__));
    }

    protected static function getUsername(): string
    {
        return static::getIdentifier();
    }
}
