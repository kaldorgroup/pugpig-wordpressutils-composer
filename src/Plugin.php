<?php

namespace Pugpig\WordPressUtils;

class Plugin
{
    /**
     * @var string $main_plugin_filepath
     */
    protected static $main_plugin_filepath;
    const DEFAULT_OPTIONS = [];
    const POST_TYPES_TO_ENSURE_ALLOWED = [];

    const ALLOW_NETWORK_ACTTIVATION = false;
    const NETWORK_ACTTIVATION_CONFIGURES_DEFAULTS_ON_ALL_SITES = false;
    const NETWORK_ACTTIVATION_FLUSHES_ON_ALL_SITES = false;

    public static function init(string $main_plugin_filepath): void
    {
        static::$main_plugin_filepath = $main_plugin_filepath;

        $class = get_called_class();
        register_activation_hook($main_plugin_filepath, [$class, 'activation']);
        register_deactivation_hook($main_plugin_filepath, [$class, 'deactivation']);
        register_uninstall_hook($main_plugin_filepath, [$class, 'uninstall']);

        add_action('wpmu_new_blog', [$class, 'newBlog'], 10, 6);
    }

    public static function activation(?bool $network_wide): void
    {
        if ($network_wide) {
            if (!static::ALLOW_NETWORK_ACTTIVATION) {
                throw new \Exception('Plugin is not allowed to be network activated');
            }

            if (static::NETWORK_ACTTIVATION_CONFIGURES_DEFAULTS_ON_ALL_SITES) {
                static::configureDefaultsOnAllSites();
            }

            if (static::NETWORK_ACTTIVATION_FLUSHES_ON_ALL_SITES) {
                SiteUtils::onEachSite(function() {
                    flush_rewrite_rules();
                });
            }
        } else {
            static::configureDefaults();
            flush_rewrite_rules();
        }
    }

    public static function deactivation(): void
    {
        flush_rewrite_rules();
    }

    public static function uninstall(): void
    {
        flush_rewrite_rules();
    }

    protected static function configureDefaultsOnAllSites(): void
    {
        SiteUtils::onEachSite(function() {
            $function_name = get_called_class() . '::configureDefaults';
            $function_name();
        });
    }

    /**
     * @param string[] $meta
     */
    public static function newBlog(int $blog_id, int $user_id, string $domain, string $path, int $site_id, array $meta): void
    {
        if (static::NETWORK_ACTTIVATION_CONFIGURES_DEFAULTS_ON_ALL_SITES
            && static::isNetworkActive()
        ) {
            switch_to_blog($blog_id);
            static::configureDefaults();
            restore_current_blog();
        }
    }

    protected static function isNetworkActive(): bool
    {
        if (!function_exists('\is_plugin_active_for_network')) {
            require_once(ABSPATH . '/wp-admin/includes/plugin.php');
        }

        return is_plugin_active_for_network(static::$main_plugin_filepath);
    }

    protected static function configureDefaults(): void
    {
        $default_options = static::getDefaultOptions();

        foreach ($default_options as $option_key => $option_value) {
            update_option($option_key, $option_value);
        }

        $allowed_types_option = get_option('pugpig_opt_allowed_types');
        if (!is_string($allowed_types_option)) {
            $allowed_types_option = '';
        }

        $allowed_types = array_map(
            'trim',
            explode(',', $allowed_types_option)
        );

        $updated = false;
        foreach (static::getPostTypesToEnsureAllowed() as $type) {
            if (!in_array($type, $allowed_types)) {
                $allowed_types[] = $type;
                $updated = true;
            }
        }

        if ($updated) {
            update_option('pugpig_opt_allowed_types', implode(',', $allowed_types));
        }
    }

    /**
     * @return array<mixed>
     */
    protected static function getDefaultOptions(): array
    {
        return static::DEFAULT_OPTIONS;
    }

    /**
     * @return string[]
     */
    protected static function getPostTypesToEnsureAllowed(): array
    {
        return static::POST_TYPES_TO_ENSURE_ALLOWED;
    }
}
