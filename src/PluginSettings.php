<?php

namespace Pugpig\WordPressUtils;

abstract class PluginSettings
{
    const TITLE = 'Settings'; // override in derived class
    const SLUG = 'settings'; // override in derived class

    public static function init(): void
    {
        $class = get_called_class();
        add_action('admin_menu', [$class, 'addSettingsMenu'], 11);
        add_action('network_admin_menu', [$class, 'addSettingsMenu']);
    }

    public static function addSettingsMenu(): void
    {
        add_options_page(
            static::TITLE,
            static::TITLE,
            'manage_options',
            static::SLUG,
            [get_called_class(), 'displayPage']
        );
    }

    abstract public static function displayPage(): void;

    /**
     * @param int|string $value
     */
    protected static function getTextInput(string $key, $value): string
    {
        return <<<"HTML_INPUT_TEXT"
            <input
                name="{$key}"
                id="{$key}"
                type="text"
                value="{$value}"
                style="width: 100%"
            >
HTML_INPUT_TEXT;
    }

    /**
     * @param int|string $value
     * @param array<string, int|string> $all_values
     * @return string[]
     */
    protected static function validateAndUpdateSetting(string $key, $value, array $all_values): array
    {
        $item_save_errors = [];
        $should_save = true;

        try {
            $short_key = str_replace(static::SLUG, '', $key);
            $validate_method = get_called_class() . '::validate' . static::studly($short_key);
            if (is_callable($validate_method)) {
                $should_save = $validate_method($value, $item_save_errors, $all_values);
            }
        } catch (\Exception $e) {
            $item_save_errors[] = $e->getMessage();
            $should_save = false;
        }

        if ($should_save) {
            update_option($key, $value);
        }

        return $item_save_errors;
    }

    protected static function studly(string $text): string
    {
        return str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $text)));
    }

    /**
     * @param string[] $item_save_errors
     */
    protected static function getSettingHtml(string $key, string $label, array $item_save_errors, string $input_html, string $help_html = ''): string
    {
        $item_save_errors_html = implode("\n", array_map(function ($item) {
            return "<p style=\"color: #f00\">{$item}</p>";
        }, $item_save_errors));

        return <<<"HTML_SETTING"
            <th scope="row">
                <label for="{key}">
                    {$label}
                </label>
            </th>
            <td>
                $input_html
                $item_save_errors_html
                $help_html
            </td>
        </tr>
HTML_SETTING;
    }
}
