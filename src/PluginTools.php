<?php

namespace Pugpig\WordPressUtils;

abstract class PluginTools
{
    const TITLE = 'Tools'; // override in derived class
    const SLUG = 'tools'; // override in derived class

    public static function init(): void
    {
        $class = get_called_class();
        add_action('admin_menu', [$class, 'addMenu'], 11);
        add_action('network_admin_menu', [$class, 'addMenu']);
    }

    public static function addMenu(): void
    {
        add_submenu_page(
            'tools.php',
            static::TITLE,
            static::TITLE,
            'add_users',
            static::SLUG,
            [get_called_class(), 'displayPage']
        );
    }

    abstract public static function displayPage(): void;
}
