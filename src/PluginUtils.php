<?php

namespace Pugpig\WordPressUtils;

class PluginUtils
{
    /**
     * @return null|array<string, mixed>
     */
    public static function getPluginInfoFromFilepath(string $filepath_in_plugin): ?array
    {
        $plugin_root = static::gePluginRootDirectory($filepath_in_plugin);

        return is_null($plugin_root)
            ? null
            : static::getPluginInfoByDirectory($plugin_root);
    }

    /**
     * @return null|array<string, mixed>
     */
    public static function getPluginInfoByDirectory(string $plugin_root_directory)
    {
        /** @var array<string, mixed> $plugin_info */
        foreach (get_plugins() as $plugin_filename => $plugin_info) {
            if (static::getRootDirectory($plugin_filename) === $plugin_root_directory) {
                $plugin_info['filepath'] = $plugin_filename;
                return $plugin_info;
            }
        }
        return null;
    }

    /**
     * @return null|array<string, mixed>
     */
    public static function getPluginInfoByMainFileLeafname(string $main_file_leafname): ?array
    {
        /** @var array<string, mixed> $plugin_info */
        foreach (get_plugins() as $plugin_filename => $plugin_info) {
            if (basename($plugin_filename) === $main_file_leafname) {
                $plugin_info['filepath'] = $plugin_filename;
                return $plugin_info;
            }
        }
        return null;
    }

    public static function gePluginRootDirectory(string $filepath_in_plugin): ?string
    {
        return static::getRootDirectory(plugin_basename($filepath_in_plugin));
    }

    protected static function getRootDirectory(?string $filepath): ?string
    {
        return is_null($filepath)
            ? null
            : current(explode(DIRECTORY_SEPARATOR, $filepath));
    }
}