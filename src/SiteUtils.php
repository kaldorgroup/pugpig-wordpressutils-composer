<?php

namespace Pugpig\WordPressUtils;

class SiteUtils
{
    const SITES_PAGE_SIZE = 100;

    /**
     * @return mixed
     */
    public static function onEachSite(callable $callback)
    {
        return static::forEachSite(function ($site) use ($callback) {
            return static::onSite($site->blog_id, $callback);
        });
    }

    /**
     * @return mixed
     */
    public static function onSite(int $blog_id, callable $callback)
    {
        switch_to_blog($blog_id);
        $response = $callback();
        restore_current_blog();
        return $response;
    }

    /**
     * @return array<mixed>
     */
    public static function forEachSite(callable $callback)
    {
        /** @var int $num_sites */
        $num_sites = get_sites(['count' => true]);
        $num_sites_handled = 0;
        $result = [];
        while ($num_sites_handled < $num_sites) {
            $sites_chunk = get_sites([
                'number' => static::SITES_PAGE_SIZE,
                'offset' => $num_sites_handled]);
            foreach ($sites_chunk as $site) {
                $result[$site->blog_id] = $callback($site);
                $num_sites_handled++;
            }
        }

        return $result;
    }

    public static function getSiteType(): string
    {
        $site_url = get_site_url();
        foreach (['dev', 'stage', 'prod'] as $type) {
            if (preg_match("/\.{$type}(?:\..*)?$/i", $site_url)) {
                return $type;
            }
        }
        return 'dev';
    }

    public static function isLocalDev(): bool
    {
        $site_url = get_site_url();
        return !!preg_match('/.*\.(loc|test|pugpig-local.com)$/i', $site_url);
    }

    public static function getSiteName(?int $blog_id = null): ?string
    {
        if (is_null($blog_id)) {
            $blog_id = get_current_blog_id();
        }

        $site = get_site($blog_id);

        return is_null($site)
            ? null
            : current(explode('.', $site->domain));
    }

    public static function getBlogIdForSitename(string $sitename): ?int
    {
        $sites = get_sites();
        foreach ($sites as $site) {
            if (current(explode('.', $site->domain)) === $sitename) {
                return (int)$site->blog_id;
            }
        }
        return null;
    }
}
