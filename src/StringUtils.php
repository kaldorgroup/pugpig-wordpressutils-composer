<?php

namespace Pugpig\WordPressUtils;

class StringUtils
{
    public static function snake(string $value, string $delimiter = '_'): string
    {
        $updated_value = preg_replace('/\s+/u', '', ucwords($value));

        $updated_value = preg_replace('/(.)(?=[A-Z])/u', '$1' . $delimiter, $value);
        if (!is_null($updated_value)) {
            $updated_value = static::lower($updated_value);
        }
        
        return $updated_value ?? $value;
    }

    public static function lower(string $value): string
    {
        return mb_strtolower($value, 'UTF-8');
    }
}
