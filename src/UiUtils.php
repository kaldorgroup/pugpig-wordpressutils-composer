<?php

namespace Pugpig\WordPressUtils;

class UiUtils
{
    public static function startBox(string $title, string $outer_class = ''): string
    {
        return <<<"BLOCK_HTML_BOX_START"
<div class="postbox {$outer_class}" style="margin: 20px 20px 20px 0;">
    <h3 class="hndle" style="margin-left: 20px;">
        <span >{$title}</span>
    </h3>
    <div class="inside">
BLOCK_HTML_BOX_START;
    }

    public static function endBox(): string
    {
        return '</div></div>';
    }

    public static function getCodeSummary(string $code, string $heading = '', int $show_length = 128): string
    {
        $displayed_code = htmlentities($code);
        $displayed_length = strlen($displayed_code);
        $displayed_snippet = substr($displayed_code, 0, $show_length);
        if ($displayed_length>$show_length) {
            $displayed_snippet.='&hellip;';
        }

        $heading_prefix = empty($heading)
            ? ''
            : "{$heading}: ";

        return "<details><summary>{$heading_prefix}<code>{$displayed_snippet}</code></summary><p><code>{$displayed_code}</code></p></details>";
    }
}
