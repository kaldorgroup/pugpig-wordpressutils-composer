<?php

namespace Pugpig\WordPressUtils;

use SimpleXMLElement;
use DOMNode;
use DOMXPath;

class XmlUtils
{
    const ATOM_NAMESPACES = [
        'atom' => 'http://www.w3.org/2005/Atom',
        'app' => 'http://www.w3.org/2007/app',
        'dcterms' => 'http://purl.org/dc/terms/',
        'opds' => 'http://opds-spec.org/2010/catalog',
        'at' => 'http://purl.org/atompub/tombstones/1.0',
        'pp' => 'http://pugpig.com/2014/atom/',
    ];

    public static function getAtom(string $contents): ?SimpleXMLElement
    {
        $xml = static::getXml($contents);
        if (!is_null($xml)) {
            $xml = static::registerAtomNamespaces($xml);
        }

        return $xml;
    }

    public static function registerAtomNamespaces(SimpleXMLElement $xml): SimpleXMLElement
    {
        foreach (static::ATOM_NAMESPACES as $key => $url) {
            $xml->registerXPathNamespace($key, $url);
        }
        return $xml;
    }

    public static function isXml(string $contents): bool
    {
        return static::getXml($contents) !== null;
    }

    public static function getXml(string $contents): ?SimpleXMLElement
    {
        $xml = null;

        $error_message = null;
        $previous_use_internal_errors = libxml_use_internal_errors(true);

        $previous_disable_entities = null;
        if (\PHP_VERSION_ID < 80000) {
            // this happens automatically in PHP >= 8
            $previous_disable_entities = libxml_disable_entity_loader(true);
        }

        libxml_clear_errors();

        try {
            $xml = new \SimpleXMLElement((string) $contents, LIBXML_NONET);
            $error = libxml_get_last_error();
            if ($error) {
                $error_message = $error->message;
                $xml = null;
            }
        } catch (\Exception $e) {
            $error_message = $e->getMessage();
        }

        libxml_clear_errors();
        libxml_use_internal_errors($previous_use_internal_errors);
        
        if (\PHP_VERSION_ID < 80000 && !is_null($previous_disable_entities)) { // @phpstan-ignore-line
            libxml_disable_entity_loader($previous_disable_entities);
        }

        return $xml;
    }

    public static function elementAsText(SimpleXMLElement $simple_xml_element): ?string
    {
        $text = null;
        if (!empty($simple_xml_element)) {
            $dom = new \DOMDocument('1.0');
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;

            $dom_node = dom_import_simplexml($simple_xml_element);
            if ($dom_node === false || is_null($dom_node)) { // @phpstan-ignore-line php7/php8 different returns
                throw new \Exception('Cannot import node');
            }
            $node = $dom->importNode($dom_node, true);
            $dom->appendChild($node);
            $xml = $dom->saveXML();
            if ($xml !== false) {
                $text = static::format($xml);
            }
        }

        return $text;
    }

    public static function format(string $xml_text): ?string
    {
        if (!$xml_text) {
            return null;
        }

        $dom = new \DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xml_text);

        $text = $dom->saveXML();
        return $text === false
            ? null
            : $text;
    }

    public static function getDomDocument(SimpleXMLElement $simple_xml_element): \DomDocument
    {
        $dom = new \DOMDocument('1.0');
        $dom_node = dom_import_simplexml($simple_xml_element);
        if ($dom_node === false || is_null($dom_node)) { // @phpstan-ignore-line php7/php8 different returns
            throw new \Exception('Cannot import node');
        }
        $node = $dom->importNode($dom_node, true);
        $dom->appendChild($node);

        return $dom;
    }

    public static function getDomXPath(SimpleXMLElement $simple_xml_element): DOMXPath
    {
        $dom = static::getDomDocument($simple_xml_element);
        $xpath = new DOMXPath($dom);

        return $xpath;
    }

    public static function getAtomDomXPath(SimpleXMLElement $simple_xml_element): DOMXPath
    {
        $xpath = static::getDomXPath($simple_xml_element);
        $xpath = static::registerAtomNamespacesForDomXPath($xpath);

        return $xpath;
    }

    public static function registerAtomNamespacesForDomXPath(\DOMXPath $xpath): DOMXPath
    {
        foreach (static::ATOM_NAMESPACES as $key => $url) {
            $xpath->registerNamespace($key, $url);
        }
        return $xpath;
    }

    public static function insertAsFirstChild(SimpleXMLElement $insert, SimpleXMLElement $target): ?DOMNode
    {
        $target_dom = dom_import_simplexml($target);
        if ($target_dom === false || is_null($target_dom)) { // @phpstan-ignore-line php7/php8 different returns
            throw new \Exception('Cannot create target dom');
        }
        $insert_dom = dom_import_simplexml($insert);
        if ($insert_dom === false || is_null($insert_dom)) { // @phpstan-ignore-line php7/php8 different returns
            throw new \Exception('Cannot create insert dom');
        }
        if (is_null($target_dom->ownerDocument)) {
            return null;
        }
        $insert_dom = $target_dom->ownerDocument->importNode($insert_dom, true);

        $first_child = $target_dom->firstChild;
        if ($first_child && !is_null($first_child->parentNode)) {
            return $first_child->parentNode->insertBefore($insert_dom, $first_child);
        }
        return $target_dom->appendChild($insert_dom); 
    }
}
