<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Pugpig\WordPressUtils\LogArray;

final class ArrayLogTest extends TestCase
{
    public function testCanLogAndGetLoggedItems(): void
    {
		$logger = new LogArray();
		$logger->info('hello');

        $this->assertEqualsCanonicalizing(
            expected: [
            	[
            		'status' => 'info',
            		'message' => 'hello',
                    'context' => [],
            	]
            ],
            actual: $logger->lines()
        );
    }
}
